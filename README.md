A basic example of using Mockito with final classes, in particular with Javalin Context objects.
Designed a reference project for Javalin Documentation.

TLDR:
I want to mock final objects with Mockito 2 or greater:

```
$> cd [MY JAVA APPLICATION PROJECT FOLDER WITH MAVEN TREE STRUCTURE]
$> touch test/resources/mockito-extensions/org.mockito.plugins.MockMaker
$> echo "mock-maker-inline" > test/resources/mockito-extensions/org.mockito.plugins.MockMaker
```
