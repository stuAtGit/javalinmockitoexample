package com.shareplaylearn.httphandlers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shareplaylearn.services.TeapotService;
import io.javalin.http.Context;

/**
 * Handles request to the teapot.
 * Note that due to the dearth of clients, it violates the RFC in one key way: it does not
 * require the use of the message/teapot media type in the header.
 * This exception was based on Postel's Law:
 * https://en.wikipedia.org/wiki/Robustness_principle
 */
public class TeapotRequestHandler {
    private final TeapotService teapotService;
    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public TeapotRequestHandler(TeapotService teapotService) {
        this.teapotService = teapotService;
    }

    public void handleGetTeaRoom(Context context) {
        context.status(200);
        context.result(gson.toJson(this.teapotService.getRoom()));
    }

    public void handlePostEarlGreyHot(Context context) {
        context.status(200);
        context.result(gson.toJson(this.teapotService.brewEarlGrey(TeapotService.Temperature.HOT)));
    }

    public void handlePostEarlGreyCold(Context context) {
        try {
            this.teapotService.brewEarlGrey(TeapotService.Temperature.COLD);
        } catch(IllegalArgumentException e) {
            context.status(400);
            context.result(e.getMessage());
        }
    }

    /**
     * Handles requests to pour coffee, per HTCPCP-TEA RFC 2324
     * @param context
     */
    public void handleBrewCoffee(Context context) {
        context.status(418);
        context.result("I'm a teapot!");
    }
}
