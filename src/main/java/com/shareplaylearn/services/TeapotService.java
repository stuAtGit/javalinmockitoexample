package com.shareplaylearn.services;

public class TeapotService {
    public enum Temperature {
        COLD,
        HOT
    }
    public String getRoom() {
        return "Your teapot is in room 418.";
    }

    public String brewEarlGrey(Temperature temperature) {
        /*
         * Brewing of cold Earl Grey is not support, per:
         * https://memory-alpha.fandom.com/wiki/Earl_Grey_tea
         */
        if(temperature.equals(Temperature.COLD)) {
            throw new IllegalArgumentException("Invalid temperature for Earl Grey. One should only request" +
                " 'Tea, Earl Grey, Hot', see https://memory-alpha.fandom.com/wiki/Earl_Grey_tea for more details.");
        }
        return "Starting brew of Earl Grey tea.";
    }
}
