package com.shareplaylearn;

import com.shareplaylearn.httphandlers.TeapotRequestHandler;
import com.shareplaylearn.services.TeapotService;
import io.javalin.Javalin;

public class MockitoJavalin {
    public static void main(String[] argv) {
        Javalin app = Javalin.create().start(8080);
        TeapotService teapotService = new TeapotService();
        TeapotRequestHandler teapotRequestHandler = new TeapotRequestHandler(teapotService);
        app.get("/tearoom", teapotRequestHandler::handleGetTeaRoom);
        app.post("/earlgrey/hot", teapotRequestHandler::handlePostEarlGreyHot);
        app.post("/earlgrey/cold", teapotRequestHandler::handlePostEarlGreyCold);
        app.post("/coffee", teapotRequestHandler::handleBrewCoffee);
    }
}
