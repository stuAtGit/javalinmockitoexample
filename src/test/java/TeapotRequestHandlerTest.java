import com.shareplaylearn.httphandlers.TeapotRequestHandler;
import com.shareplaylearn.services.TeapotService;
import io.javalin.http.Context;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

/**
 * Note, the inline MockMaker needs to be enabled for these tests to work.
 * Otherwise, you'll get errors like so:
 *
 * java.lang.NullPointerException
 * 	at io.javalin.http.Context.status(Context.kt:386)
 * 	at com.shareplaylearn.httphandlers.TeapotRequestHandler.handleBrewCoffee(TeapotRequestHandler.java:47)
 * 	at TeapotRequestHandlerTest.handleBrewCoffee(TeapotRequestHandlerTest.java:21)
 * 	...
 **/
public class TeapotRequestHandlerTest {
    private TeapotRequestHandler teapotRequestHandler;
    private TeapotService teapotService;

    public TeapotRequestHandlerTest() {
        this.teapotService = mock(TeapotService.class);
        this.teapotRequestHandler = new TeapotRequestHandler(this.teapotService);
    }

    @Test
    public void handleBrewCoffee() {
        Context context = mock(Context.class);
        this.teapotRequestHandler.handleBrewCoffee(context);
        verify(context).status(418);
        verify(context).result("I'm a teapot!");
    }

    @Test
    public void handleBrewEarlGreyHot() {
        Context context = mock(Context.class);
        this.teapotRequestHandler.handlePostEarlGreyHot(context);
        verify(context).status(200);
    }

    @Test
    public void handleBrewEarlGreyCold() {
        Context context = mock(Context.class);
        when(this.teapotService.brewEarlGrey(eq(TeapotService.Temperature.COLD))).thenThrow(IllegalArgumentException.class);
        this.teapotRequestHandler.handlePostEarlGreyCold(context);
        verify(context).status(400);
    }
}
